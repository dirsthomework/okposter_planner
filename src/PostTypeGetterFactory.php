<?php

namespace Dirst\OkPoster;

use Dirst\OkTools\OkToolsClient;
use MeekroDB;

/**
 * Description of PostTypeGetterFactory
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class PostTypeGetterFactory
{
    /**
     * Constructs new PostGetterBase object.
     *
     * @param int $postId
     *   Current future Post id in BO.
     * @param int $taskId
     *   Task Id on behalf post has been ran. 
     * @param string $postType
     *   Post type getter name.
     * @param MeekroDb $db
     *   MeekroDb object.
     * @param string $groupTo
     *   Group id to post to.
     * @param array $postTypeSettings
     *   Post type settings.
     * @param OkToolsClient $client
     *   OkTools client if required.
     * 
     * @return PostGetterBase
     *   PostGetterInterface object.
     */
    static public function produce(
        $postId,
        $taskId,
        $postType,
        MeekroDB $db,
        $groupTo,
        array $postTypeSettings = [],
        OkToolsClient $client = null
    ) {
        switch($postType) {
          case "okposter_borrow_post_type":
            return new BorrowPostTypeGetter($postId, $taskId, $postType, $db, $groupTo, $postTypeSettings, $client);
            break;
        }
    }
}
