<?php

namespace Dirst\OkPoster;

use Katzgrau\KLogger\Logger;
use Psr\Log\LogLevel;
use Dirst\OkTools\Groups\OkToolsGroupsControl;
use Dirst\OkTools\Exceptions\OkToolsBlockedUserException;
use Dirst\OkTools\Exceptions\Group\OkToolsGroupPostTopicException;

/**
 * Methods to use in planner routines.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class OkPosterBase
{
    // Storage folder - accounts session, key, token, etc.
    const LOGIN_STORAGE = __DIR__ . "/login_data";
    
    // @var string Log dir.
    protected $logDir = __DIR__ . "/../logs";
    
    // @var Logger object..
    protected $logger;

    // @var LogLevel object thresold.
    protected $logLevelThresold;

    // @var DatabaseClient object. 
    protected $dbClient;
    
    // @var int seconds limit for posting.
    protected $postingTimeLimit = 60;

    /**
     * Construct Poster object. Set up logger and DB connection.
     *
     * @param int
     *   Minimum possible log level - thresold.
     */
    public function __construct($logLevelThresold = LogLevel::DEBUG)
    {

        // Set logger.
        $this->logLevelThresold = $logLevelThresold;
        $this->logger = new Logger($this->logDir, $this->logLevelThresold);

        // Set up database connection.
        $this->dbClient = new DataBaseClient();
        
        // Adds login data storage folder     
        if (!is_dir(self::LOGIN_STORAGE) && !mkdir(self::LOGIN_STORAGE)) {
            $this->logger->info("Couldn't create login storage folder.");
            exit;
        }
    }

    /**
     * Run posts process.
     */
    public function run()
    {
      try {
          // Get prepared tasks.
          $postsObject = new Posts($this->dbClient->getDatabaseConnection(), $this->logger);
          $preparedPosts = $postsObject->getPreparedPosts();
          
          // Check if posts exists.
          if (!$preparedPosts) {
            $this->logger->alert("No available posts have been found.", [$preparedPosts]);
            return;
          }

          $this->logger->info("==========================\nStart with new posting session.");
      }
      catch (\Exception $exception) {
          $this->logger->alert("Problem has been occured on posts array retrieve.", [$exception->getMessage()]);
          return;
      }

      // Do posting for every retireved post.
      $time = time();
      foreach ($preparedPosts as &$post) {
          // Time limit.
          if ($time + $this->postingTimeLimit <= time()) {
              break;
          }
          $this->postingHandler($post);
      }

      $this->logger->info("==========================\nPosting session is over.");
    }
    
    /**
     * Run posting and handle exceptions.
     *
     * @param array $preparedPost
     *   Prepared post array.
     */
    protected function postingHandler(array &$preparedPost)
    {
        try {
            $this->posting($preparedPost);
        }
        catch (OkToolsBlockedUserException $exception) {
            $fullLogName = "details_" . date("d-M-Y H:i:s") . "txt";
            $this->logger->alert("Account has been blocked. Check $fullLogName", [$exception->getMessage()]);
            file_put_contents($this->logDir . "/" . $fullLogName, $exception);

            $this->setPostStatus(Posts::POST_PREPARED_STATUS, $preparedPost['post_id']);
            $this->disableAccountByLogin($exception->itemId);
        }
        catch (OkToolsGroupPostTopicException $exception) {
            $fullLogName = "details_" . date("d-M-Y H:i:s") . "txt";
            $this->logger->alert("Post has been failed. Check $fullLogName", [$exception->getMessage()]);
            file_put_contents($this->logDir . "/" . $fullLogName, $exception);

            $this->setPostStatus(Posts::POST_FAILED_STATUS, $preparedPost['post_id']);
        }
        catch (\Exception $exception) {
            $fullLogName = "details_" . date("d-M-Y H:i:s") . "txt";
            $this->logger->alert("Problem has been occured on posting process. Check $fullLogName", [$exception->getMessage()]);
            file_put_contents($this->logDir . "/" . $fullLogName, $exception);

            $this->setPostStatus(Posts::POST_PREPARED_STATUS, $preparedPost['post_id']);
            return;
        }
    }

    /**
     * Make posting.
     *
     * @param array $preparedPost
     *   Prepared post array according to db.
     */
    protected function posting(array &$preparedPost)
    {
        // Run.
        $this->setPostStatus(Posts::POST_RAN_STATUS, $preparedPost['post_id']);
      
        // Get Account attached to post.
        $accountPrepare = new AccountPrepare($this->dbClient->getDatabaseConnection(), $preparedPost['posting_account_id']);
        $account = $accountPrepare->getAccount();
        $this->logger->info("Posting account retireved for topic id {$preparedPost['post_id']}");
        
        // Update account info.
        $accountPrepare->updateAccountInfo($account);
        
        // Authorize posting account. Exceptions activation, block handling
        $clientPrepare = new ClientPrepare($this->dbClient->getDatabaseConnection(), $this->logger);
        $client = $clientPrepare->getPreparedInviter($account, $preparedPost['post_to_group']);

        // Create new posting type getter.
        $postTypeGetter = PostTypeGetterFactory::produce(
            $preparedPost['post_id'],
            $preparedPost['id'],
            $preparedPost['posting_type'],
            $this->dbClient->getDatabaseConnection(),
            $preparedPost['post_to_group'],
            unserialize($preparedPost['settings']),
            $client
        );

        // Retrieve new post.
        $postData = $postTypeGetter->getPostData();
        $this->logger->info("Post data have been retrieved with success for topic id {$preparedPost['post_id']}");
        
        // Make posting.
        $group = new OkToolsGroupsControl($client, $preparedPost['post_to_group']);
        
        // Post and check if success.
        $group->postMediaTopic($postData);
        $this->logger->info("New topic has been posted in group {$preparedPost['post_to_group']}");

        // Set post source id.
        if ($sourceId = $postTypeGetter->getSourceId()) {
            $this->setPostSourceId($sourceId, $preparedPost['post_id']);
        }

        // Done status.
        $this->setPostStatus(Posts::POST_DONE_STATUS, $preparedPost['post_id']);
        $this->logger->info("Posting is done for topic id {$preparedPost['post_id']}");
    }

    /**
     * Disable Account and Remove it from stack.
     *
     * @param string
     *   Account login.
     */
    protected function disableAccountByLogin($login)
    {
        // Disable account.
        $this->dbClient->getDatabaseConnection()->update(
            DataBaseClient::ACCOUNTS_TABLE,
            [
                'disabled' => 1
            ],
            'login = %s',
            $login
        );

        $this->logger->warning("Account {$login} has been disabled.");
    }
    
    /**
     * Set post status.
     *
     * @param int $status
     *   Post status to set.
     * @param int $postId
     *   Post id.
     */
    protected function setPostStatus($status, $postId) {
        // Change post status.
        $this->dbClient->getDatabaseConnection()->update(
            DataBaseClient::POSTS_TABLE,
            [
                "status" => $status
            ],
            'id = %i',
            $postId
        );
    }
 
    /**
     * Update post source id.
     *
     * @param string $sourceId
     *   Source Id.
     * @param int $postId
     *   Post Id to change.
     */
    protected function setPostSourceId($sourceId, $postId)
    {
        // Change source id of the post.
        $this->dbClient->getDatabaseConnection()->update(
            DataBaseClient::POSTS_TABLE,
            [
                "source_id" => $sourceId
            ],
            'id = %i',
            $postId
        );
    }
}
