<?php

namespace Dirst\OkPoster;

/**
 * Base poster Exception.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class OkPosterException extends \Exception
{
}
