<?php

namespace Dirst\OkPoster;

use Dirst\OkTools\OkToolsClient;
use MeekroDB;

/**
 * Base class for post getter types.
 * 
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
abstract class PostGetterBase
{
    // @var array type settings.
    protected $postTypeSettings;
    
    // @var OkToolsClient client.
    protected $client;

    // @var MeekroDb object.
    protected $db;
    
    // @var string Post type.
    protected $postType;
    
    // @var string source id.
    protected $sourceId;
    
    // @var string id of the group to post to.
    protected $groupTo;
    
    // @var int task id.
    protected $taskId;
    
    // @var int current post id.
    protected $currentPostId;

    /**
     * Retrieve ready to post data according to post type class.
     *
     * @return array
     *   Data in OK format to post.
     */
    abstract public function getPostData();

    /**
     * Retrieve source post Id.
     * 
     * @return string
     *   Source Id.
     */
    public function getSourceId()
    {
        return $this->sourceId;
    }

    /**
     * Constructs new post type getter.
     *
     * @param int $postId
     *   Current future Post id in BO.
     * @param int $taskId
     *   Task Id on behalf post has been ran. 
     * @param string $postType
     *   Post type getter name.
     * @param MeekroDb $db
     *   MeekroDb object.
     * @param string $groupTo
     *   Group id to post to.
     * @param array $postTypeSettings
     *   Post type settings.
     * @param OkToolsClient $client
     *   OkTools client if required.
     */
    public function __construct(
        $postId,
        $taskId,
        $postType,
        MeekroDB $db,
        $groupTo,
        array $postTypeSettings = [],
        OkToolsClient $client = null
    ) {
        $this->postTypeSettings = $postTypeSettings;
        $this->client = $client;
        $this->db = $db;
        $this->postType = $postType;
        $this->groupTo = $groupTo;
        $this->currentPostId = $postId;
        $this->taskId = $taskId;
    }
}
