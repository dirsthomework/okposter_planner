<?php

namespace Dirst\OkPoster;

use Dirst\OkTools\OkToolsClient;
use MeekroDB;
use Psr\Log\AbstractLogger;
use OkToolsUnfreeze;
use Dirst\OkTools\Groups\OkToolsGroupsControl;
use Dirst\OkTools\Groups\OkToolsGroupRoleEnum;
use Dirst\OkTools\Requesters\RequestersTypesEnum;

use Dirst\OkTools\Exceptions\OkToolsBlockedException;

/**
 * Client prepare class.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class ClientPrepare
{
    // OK Application key.
    const APP_KEY = "CBAFJIICABABABABA";

    // Activate sms keys.
    const SMS_ACTIVATE_KEY = "6A6cA2c3592d98b5b9284e34bd2A7223";
    const GET_SMS_KEY = "XR6BKJLHTKUTECC1OD4DTZ76U6Z5TGLG";
    
    // @var MeekroDB object.
    protected $db;
    
    // @var AbstractLogger class object.
    protected $logger;
    
    /**
     * Construct object.
     *
     * @param MeekroDB $db
     *   Database object.
     * @param AbstractLogger $logger
     *   Logger object.
     */
    public function __construct(MeekroDB $db, AbstractLogger $logger)
    {
        $this->db = $db;
        $this->logger = $logger;
    }
    
    /**
     * Join the group. Assign moderator role And return client object.
     *
     * @param array $account
     *   Account array according to db table.
     * @param int $targetGroupId
     *   Target group Id - donor.
     *
     * @throws OkPosterException
     *   Thrown if account couldn't join the group.
     *
     * @return OkToolsClient
     *   Return Inviter client object.
     */
    public function getPreparedInviter(array &$account, $targetGroupId)
    {   
        // Login.
        $client = $this->login($account);
        
        // Join.
        $this->joinTheGroup($client, $targetGroupId);
        
        // Sleep on group page.
        sleep(rand(2, 3));
        
        // Grant moderator access.
        $this->grantModeratorRights($account, $client, $targetGroupId);

        return $client;
    }
    
    /**
     * Join the group procedure.
     *
     * @param OkToolsClient $client
     *   Client to use for group control init.
     * @param int $targetGroupId
     *   Target group Id - donor.
     *
     * @throws OkPosterException
     *   If not able to join the group.
     */
    protected function joinTheGroup(OkToolsClient $client, $targetGroupId)
    {
        // Init group control.
        $okToolsGroupControl = new OkToolsGroupsControl($client, $targetGroupId);

        // Join group acceptor with current account.
        if (!$okToolsGroupControl->isJoinedToGroup()) {
            $okToolsGroupControl->joinTheGroup();
            if (!$okToolsGroupControl->isJoinedToGroup()) {
                throw new OkPosterException("Couldn't join the group {$targetGroupId}");
            } else {
                $this->logger->info("Joined to group {$targetGroupId}");
            }
        }
    }

    /**
     * Grant moderator access.
     *
     * @param array $account
     *   Account array.
     * @param OkToolsClient $client
     *   OkTools Client
     * @param int $targetGroupId
     *   Target group id.
     */
    protected function grantModeratorRights(array &$account, OkToolsClient $client, $targetGroupId)
    {
        // Assign moderator permission for this account.
        if (!is_array($account['moderated_groups']) || !in_array($targetGroupId, $account['moderated_groups'])) {
            $this->assignModeratorRole($client->getAccountApiId(), $targetGroupId);
            
            // Add group to account moderated groups.
            $this->addGroupToModerated($account, $targetGroupId);
            $this->logger->info("Moderator role has been assigned in {$targetGroupId} for account id = {$account['id']}");
        } else {
            $this->logger->info("Account is moderator already for the group {$targetGroupId} id = {$account['id']}");
        }
    }

    /**
     * Assign moderator role.
     *
     * @param int $okAccountId
     *   Profile Id in social network.
     * @param int $groupId
     *   Group Id in social network.
     */
    protected function assignModeratorRole($okAccountId, $groupId)
    {
        // Prepare inviter.
        $login = ModeratorForAll::LOGIN;
        $pass = ModeratorForAll::PASS;
        $client = new OkToolsClient(
            $login,
            $pass,
            RequestersTypesEnum::CURL(),
            ModeratorForAll::INSTALL_ID,
            ModeratorForAll::DEVICE_ID,
            ModeratorForAll::ANDROID_ID,
            self::APP_KEY,
            null,
            ModeratorForAll::UA_DEVICE,
            ModeratorForAll::UA,
            OkPosterBase::LOGIN_STORAGE
        );
        $moderatorGroupControl = new OkToolsGroupsControl($client, $groupId);
        $this->logger->info("Supermoderator has been authorzed for {$groupId}", [ModeratorForAll::LOGIN]);

        // Assign role to Account.
        $moderatorGroupControl->assignGroupRole(OkToolsGroupRoleEnum::MODERATOR(), $okAccountId);
    }

    /**
     * Add group to moderated list in account.
     * 
     * @param array $account
     *   Account array.
     * @param int $groupId
     *   Group ID to add.
     */
    protected function addGroupToModerated(&$account, $groupId) {
        // Init array if no value is setted.
        if (!isset($account['moderated_groups']) || !$account['moderated_groups']) {
            $account['moderated_groups'] = [];
        }
        
        // Add group Id.
        if (!in_array($groupId, $account['moderated_groups'])) {
            $account['moderated_groups'][] = $groupId;

            // Update moderated groups. 
            $this->db->update(
                DataBaseClient::ACCOUNTS_TABLE,
                [
                    "moderated_groups" => serialize($account['moderated_groups']),
                ],
                'id = %i',
                $account['id']
            );
        } else {
            $this->logger->info("{$groupId} is already in moderated list for this account id = {$account['id']}");
        }
    }

    /**
     * Trying to login.
     * 
     * @param array $account
     *   Account array.
     *
     * @throws OkToolsBlockedUserException
     *   User blocked exception.
     * 
     * @return OkToolsClient
     *   Return Inviter client object.
     */
    protected function login(array &$account)
    {
        // Init tools client object.
        login:
        try {
          $client = new OkToolsClient(
              $account['login'],
              $account['pass'],
              RequestersTypesEnum::CURL(),
              $account['install_id'],
              $account['device_id'],
              $account['android_id'],
              self::APP_KEY,
              $account['proxy'],
              $account['useragent_mobile'],
              $account['useragent'],
              OkPosterBase::LOGIN_STORAGE
          );
        } catch(OkToolsBlockedUserException $ex) {
            // Unfreeze account if it is for the first time.
            if (!$account['unfreeze_count']) {
                $this->unfreezeAccount($account, $ex->verificationUrl);

                // Tryring to login again.
                goto login;
            } else {
                // throw to upper level if not first time.
                throw $ex;
            }
        }
        $this->logger->info("Authorization of invite account has been done id = {$account['id']}");
        
        return $client;
    }
 
    /**
     * Unfreeze account.
     * 
     * @TODO ACtivation service random select.
     *
     * @param array $account
     *   Account array from database to unfreeze.
     * @param string $verificationUrl
     *   Verification url.
     */
    protected function unfreezeAccount(array &$account, $verificationUrl) {
        $this->logger->info("Account is going to be unfreezed  for the first time id = {$account['id']}");

        $unf = new OkToolsUnfreeze(self::GET_SMS_KEY, $account['proxy'], OkToolsUnfreeze::GETSMS);
        $result = $unf->unfreezeAccount($verificationUrl);
        $account['unfreeze_count'] = $account['unfreeze_count'] ? $account['unfreeze_count'] + 1 : 1;
        // Change only if phone.
        $account['login'] = "+" . $result['phone'];
        $account['pass'] = $result['password'] ? $result['password'] : $account['pass'];

        $this->logger->info("Account has been unfreezed  for the first time id = {$account['id']}");
        // if account has been unfreezed save attempt number in db.
        $this->db->update(
            DataBaseClient::ACCOUNTS_TABLE,
            [
                "unfreeze_count" => $account['unfreeze_count'],
                "login" => $account['login'],
                "pass" => $account['pass']
            ],
            'id = %i',
            $account['id']
        );
    }
}
