<?php

namespace Dirst\OkPoster;

use Dirst\OkTools\Groups\OkToolsGroupsControl;

/**
 * Description of BorrowPostType
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class BorrowPostTypeGetter extends PostGetterBase
{
    // @var int posts count.
    protected $postsCount = 100;

    // @var OkToolsGroupsControl object of group post donor.
    protected $groupControl;
    
    // @var int number of posts to offset if on first page.
    protected $firstPageOffset = 25;
    
    // @var int group from key in groups array.
    protected $groupFromKey;
    
    /**
     * {@inheritdoc}
     */
    public function getPostData()
    {
        // Init random group by Id.
        $groupsSource = explode(PHP_EOL, $this->postTypeSettings['from_groups']);
        $this->groupFromKey = array_rand($groupsSource);
        
        // Explode to anchor. GROUPID:ANCHOR
        $groupAnchor = explode(":", $groupsSource[$this->groupFromKey]);

        // Get anchor
        $anchor = isset($groupAnchor[1]) ? $groupAnchor[1] : null;
        
        $this->groupControl = new OkToolsGroupsControl($this->client, $groupAnchor[0]);       
        $postData = $this->getMainPostData($anchor);

        // Attach links
        
        if (trim($this->postTypeSettings['add_links'])) {
            $links = explode(PHP_EOL, $this->postTypeSettings['add_links']);
            $randomLink = $links[array_rand($links)];
            $attach = [
                "type" => 'text',
                "text" => $randomLink
            ];
            switch($this->postTypeSettings['link_location']) {
                case "top":
                  array_unshift($postData, $attach);
                  break;
                default:
                  $postData[] = $attach;
            }
        }

        return $postData;
    }
    
    /**
     * Get main post data aray.
     *
     * @param string $anchor
     *   Anchor.
     * 
     * @return array
     *   Media posts array.
     */
    protected function getMainPostData($anchor = null) {
        // Select all posts from group from current page.
        $mediaTopics = $this->groupControl->getMediaTopicsFeed($this->postsCount, $anchor);
        
        // Remove posts count according offset.
        if (!isset($mediaTopics['media_topics'])) {
            throw new OkPosterException("Couldn't find any posts: " . var_export($mediaTopics, true));
        }
        
        // if no anchor slice it to offset.
        if (!$anchor) {
            $mediaTopics['media_topics'] = array_slice($mediaTopics['media_topics'], $this->firstPageOffset);
        }

        while(true) {
            if (isset($mediaTopics['media_topics']) && $postingArray = $this->getPostingArray($mediaTopics['media_topics'])) {
                return $postingArray;
            } else {
                throw new OkPosterException("Couldn't find any posts: " . var_export($mediaTopics, true));
            }

            // If no appropriate post found move to next page.
            if ($mediaTopics['has_more']) {
                $this->saveAnchorForGroup($mediaTopics['anchor']);
                $mediaTopics = $this->groupControl->getMediaTopicsFeed($this->postsCount, $mediaTopics['anchor']);
            } else {
                // if there is no more posts, get back to the 1st page.
                $this->saveAnchorForGroup();
                throw new OkPosterException("No more posts on next page. Get back to 1st page.");
            }
        }
    }

    /**
     * Save new anchor to source group settings.
     *
     * @param string $newAnchor
     *   New page anchor to save.
     */
    protected function saveAnchorForGroup($newAnchor = null)
    {
        $groupsSource = explode(PHP_EOL, $this->postTypeSettings['from_groups']);
        $groupId = explode(":", $groupsSource[$this->groupFromKey])[0];
        $groupsSource[$this->groupFromKey] = $groupId . ($newAnchor ? ":$newAnchor" : "");
        
        // Pack groups back to one per line text
        $this->postTypeSettings['from_groups'] = implode(PHP_EOL, $groupsSource);
        
        // Change source id of the post.
        $this->db->update(
            DataBaseClient::TASK_TABLE,
            [
                "settings" => serialize($this->postTypeSettings)
            ],
            'id = %i',
            $this->taskId
        );
    }
    
    /**
     * Returns posting array.
     *
     * @param array $mediaTopics
     *   Media topics list array.
     *
     * @return boolean|array
     *   Ready to be posted array or false if there is no appropriate post has been found.
     */
    protected function getPostingArray(&$mediaTopics)
    {
        // Iterate through every post, check for suitability and build ready posting array.
        foreach ($mediaTopics as $topic) {
            if (
                ( $this->checkPostIsSuitable($topic) && 
                $details = $this->getPostDetails($topic['id']) ) &&
                ( isset($details['media_topics']) &&
                @!$details['media_topics'][0]['is_promo'] &&
                !$this->checkIfLinks($details['media_topics'][0]['media']) &&
                $this->textAndPhotoOnly($details['media_topics'][0]['media']) )
            ) {
                // Build posting array.
                $post = $this->buildPostArray($details);
                if (!empty($post)) {
                    $this->sourceId = $topic['id'];
                    return $post;
                } else {
                    continue;
                }
            } else {
                continue;
            }
        }

        return false;
    }

    /**
     * Build Post array.
     *
     * @param array $topicArray
     *   Topic details array.
     *
     * @return array
     *   Post array.
     */
    protected function buildPostArray(&$topicArray)
    {
        $post = [];
        foreach ($topicArray['media_topics'][0]['media'] as $oneMedia) {
            if ($oneMedia['type'] == "text") {
                $post[] = [
                  "type" => 'text',
                  "text" => $oneMedia['text']
                ];
            } elseif ($oneMedia['type'] == 'photo') {
                $list = [];
                foreach ($oneMedia['photo_refs'] as $photoKey => $ref) {
                    $downloadUrl = $this->getPhotoUrlToDownload($topicArray['entities']['group_photos'][$photoKey]);
                    $list[] = ['id' => $this->getUploadedPhotoId($downloadUrl)];
                }
                $post[] = [
                    "type" => 'photo',
                    "list" => $list
                ];
            }
        }

        return $post;
    }

    /**
     * Get photo download url.
     *
     * @param array $photoArray
     *   Photo array.
     *
     * @return string
     *   Url to download photo.
     */
    protected function getPhotoUrlToDownload(&$photoArray)
    {
        $maxResolution = 0;
        $photoUrlField = null;
        foreach ($photoArray as $photoFieldName => $photoData) {
            if (strpos($photoFieldName, "pic") === 0) {
                $currentResolution = preg_replace("/^pic(\d+)(.+)?$/m", "$1", $photoFieldName);
                if ($currentResolution > $maxResolution) {
                    $maxResolution = $currentResolution;
                    $photoUrlField = $photoFieldName;
                }
            }
        }
        
        return $photoArray[$photoUrlField];
    }

    /**
     * Get uploaded photo url.
     *
     * @param string $photoUrl
     *   Path to photo to upload.
     *
     * @return string
     *   Id of uploaded photo.
     */
    protected function getUploadedPhotoId($photoUrl)
    {
        // Group post acceptor.
        $groupControl = new OkToolsGroupsControl($this->client, $this->groupTo);
        return $groupControl->uploadAndGetPhotoId($photoUrl);
    }
    
    /**
     * Check if topic has Text and photo only.
     *
     * @param array $media
     *   Media array.
     *
     * @return boolean
     *   True if text and photo only exist in topic.
     */
    protected function textAndPhotoOnly(&$media)
    {
        foreach ($media as $oneMedia) {
            if (!in_array($oneMedia['type'], ["text", "photo"])) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if links are exist in a topic.
     *
     * @param array $media
     *   Topic data.
     *
     * @return boolean
     *   True if topic has link.
     */
    protected function checkIfLinks(&$media)
    {
        $linkPatter = "/^.+(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?.+$/m";
        foreach($media as $oneMedia) {
            if ($oneMedia['type'] == 'text' && preg_match($linkPatter, $oneMedia['text'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get topic details.
     *
     * @param int $topicId
     *   Topic Id.
     *
     * @return array
     *   Post details.
     */
    protected function getPostDetails($topicId)
    {
        return $this->groupControl->getTopicDetails($topicId);
    }

    /**
     * Check if post is suitable for posting.
     *
     * @notice
     *   Currently posts are unique only for group to.
     *
     * @param array $topic
     *   Post array.
     *
     * @return boolean
     *  Flag - post is suitable for posting or not.
     */
    protected function checkPostIsSuitable($topic) {
        // Topic ID check if it has been posted already.
        $post = $this->db->queryFirstRow(
            "SELECT * FROM ". DatabaseClient::POSTS_TABLE . " as posts "
            . "LEFT JOIN " . DatabaseClient::TASK_TABLE . " as tasks ON posts.task_id = tasks.id "
            . "WHERE posting_type = %s AND source_id = %s AND tasks.post_to_group = %s",
            $this->postType,
            $topic['id'],
            $this->groupTo
        );
        
        // Post is already exists.
        if ($post) {
            return false;
        }
        
        // Check settings filter.
        if (
            $topic['discussion_summary']['comments_count'] >= $this->postTypeSettings['comments_count']
            &&
            $topic['like_summary']['count'] >= $this->postTypeSettings['reshare_count']
            &&
            $topic['reshare_summary']['count'] >= $this->postTypeSettings['likes_count']
        ) {
            return true;
        } else {     
            return false;
        }
    }
}
