<?php

namespace Dirst\OkPoster;

use MeekroDB;
use Psr\Log\AbstractLogger;

/**
 * Posts control class.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class Posts
{
    // @var MeekroDB object.
    protected $db;
    
    // @var AbstractLogger class object.
    protected $logger;
    
    // @var array database rows with tasks.
    protected $posts;
    
    // @var int statuses.
    const POST_PREPARED_STATUS = 0;
    const POST_RAN_STATUS = 1;
    const POST_FAILED_STATUS = 3;
    const POST_DONE_STATUS = 2;

    /**
     * Construct object.
     *
     * @param MeekroDB $db
     *   Database object.
     * @param AbstractLogger $logger
     *   Logger object.
     */
    public function __construct(MeekroDB $db, AbstractLogger $logger)
    {
        $this->db = $db;
        $this->logger = $logger;
        
        // Select posts.
        $this->posts = $this->db->query(
            "SELECT posts.post_time,posts.id as post_id,tasks.* FROM " . DataBaseClient::POSTS_TABLE . " as posts "
            . "LEFT JOIN " . DataBaseClient::TASK_TABLE . " as tasks ON posts.task_id = tasks.id "
            . "LEFT JOIN " . DatabaseClient::ACCOUNTS_TABLE . " as account ON account.id = tasks.posting_account_id "
            . "WHERE posts.status = %i AND posts.post_time <= %i AND tasks.disabled = %i AND account.disabled = %i",
            self::POST_PREPARED_STATUS,
            time(),
            0,
            0
        );
    }
    
    /**
     * Get prepared tasks.
     *
     * @return array
     *   Array of tasks ready to be posted.
     */
    public function getPreparedPosts() {
        return $this->posts;
    }
}
