<?php

namespace Dirst\OkPoster;

use MeekroDB;
use Rhumsaa\Uuid\Uuid;

/**
 * Account preparation and other helpers.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class AccountPrepare
{
    // @var MeekroDB object.
    protected $db;
  
    // @var array $account.
    protected $account;

    /**
     * Construct object.
     *
     * @param MeekroDB $db
     *   Database object.
     */
    public function __construct(MeekroDB $db, $accountId)
    {
        $this->db = $db;
        $this->account = $this->db->queryFirstRow(
            "SELECT * FROM " . DataBaseClient::ACCOUNTS_TABLE . " WHERE id = %i", $accountId
        );
    }
  
    /**
     * Get Available account.
     *
     * @param int $taskId
     *   Taask Id.
     *
     * @return array|boolean
     *   Retireved account or false.
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Update retrieved account info.
     *
     * @param array $account
     *   Account array t update.
     */
    public function updateAccountInfo(array &$account)
    {
        // Useragent assign.
        $account['useragent'] = $account['useragent'] ? $account['useragent'] : $this->randomOkBrowserUserAgent();
        $account['useragent_mobile'] = $account['useragent_mobile'] ? $account['useragent_mobile'] : $this->randmOKAndroidUserAgent();

        // Assign android ids.
        $account["install_id"] = $account["install_id"] ? $account["install_id"] : Uuid::uuid4()->toString();
        $account["device_id"] = $account["device_id"] ? $account["device_id"] : $this->randomString(range(0, 8), 15);
        $account["android_id"] = $account["android_id"] ? 
            $account["android_id"] : $this->randomString(array_merge(range(0, 9), range("a", "f")), 16);
        
        // Update account.
        $this->db->update(
          DataBaseClient::ACCOUNTS_TABLE,
          [
              "useragent" => $account['useragent'],
              "useragent_mobile" => $account['useragent_mobile'],
              "install_id" => $account["install_id"],
              "device_id" => $account["device_id"],
              "android_id" => $account["android_id"]
          ],
          'id = %i',
          $account['id']
        );

        // Unpack moderated groups.
        $account['moderated_groups'] = unserialize($account['moderated_groups']);
    }

    /**
     * Generate random string according to length and characters array.
     *
     * @param type $range
     *   Array of characters to generate string from.
     * @param int $length
     *   Length of the generated string.
     *
     * @return string
     *   Generated string
     */
    public function randomString($range, $length) {
        $str = "";
        $max = count($range) - 1;
        for ($i = 0; $i < $length; $i++) {
          $rand = mt_rand(0, $max);
          $str .= $range[$rand];
        }
        return $str;
    }

    /**
     * Return random browser useragent.
     *
     * @return string
     *   Random user agent.
     */
    public function randomOkBrowserUserAgent() {
      $android = "Android " . rand(4, 7) . "." . rand(1, 6) . "." . rand(1, 7);
      $webkit = rand(520, 547) . "." . rand(1, 32);
      $chrome = rand(30, 36) . "." . rand(0, 9) . "." . rand(0, 9) . "." . rand(0, 9);
      $mozillaVersion = rand(5, 6) . "." . rand(0, 9);
      $phone = [
          "HM NOTE 2S Build/MiuiPro",
          "Galaxy Tab A 8.0 Build/Nougat",
          "Galaxy Note8 Build/Nougat",
          "Galaxy S8 Build/Nougat",
          "Galaxy A7 Build/Marshmallow",
          "Galaxy Note FE Build/Nougat",
          "Galaxy C5 Pro Build/Nougat"
        ];
      $phoneSelect = $phone[rand(0, count($phone) - 1)];
      return "Mozilla/$mozillaVersion (Linux; $android; $phoneSelect) AppleWebKit/$webkit "
      . "(KHTML, like Gecko) Version/4.1 Chrome/$chrome Mobile Safari/$webkit OKAndroid/15.2.1 OkApp";
    }

    /**
     * Random ok android device string.
     *
     * @return string
     *   Return random ok mobile device.
     */
    public function randmOKAndroidUserAgent() {
        $android = "Android " . rand(4, 7) . "." . rand(1, 6) . "." . rand(1, 7);
        $phone = [
          "Xiaomi HM NOTE 2S Build/MiuiPro",
          "Samsung Galaxy Tab A 8.0 Build/Nougat",
          "Samsung Galaxy Note8 Build/Nougat",
          "Samsung Galaxy S8 Build/Nougat",
          "Samsung Galaxy A7 Build/Marshmallow",
          "Samsung Galaxy Note FE Build/Nougat",
          "Samsung Galaxy C5 Pro Build/Nougat"
        ];

        $resolution = [
          "640x1024",
          "768×1280",
          "720×1280",
          "720×1280",
          "1280×720",
          "1920×1200",
          "1280×720"
        ];
        
        $phoneSelect = $phone[rand(0, count($phone) - 1)];
        $resolutionSelect = $resolution[rand(0, count($resolution) - 1)];
        $dpi = rand(320, 365);
        return "OKAndroid/15.2.1 b319 ($android; ru_RU; $phoneSelect; xhdpi {$dpi}dpi $resolutionSelect)"; 
    }
}
