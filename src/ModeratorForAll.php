<?php

namespace Dirst\OkPoster;

/**
 * SUpermoder account. Will be used to assign moderator permissions to all accounts.
 * Later there will be backend interface for that.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class ModeratorForAll
{
    const LOGIN = "+79252343106";
    const PASS  = "umA7Esk10AmI1kaJ";
    const INSTALL_ID = "12affa243-31f1-4615-be2d-cd1d9f1a3acd";
    const DEVICE_ID = "865433532300833";
    const ANDROID_ID = "c146a3111fbaec1e";
    const UA = "Mozilla/5.0 (Linux; Android 4.3.4; HM NOTE 1S Build/KTU84P) AppleWebKit/537.21 "
      . "(KHTML, like Gecko) Version/4.1 Chrome/33.0.0.0 Mobile Safari/527.24 OKAndroid/11.1.5 OkApp";
    const UA_DEVICE = "OKAndroid/15.2.1 b319 (Android 4.1.4; ru_RU; Xiaomi HM NOTE 1S Build/MiuiPro; xhdpi 320dpi 640x1024)";
}
