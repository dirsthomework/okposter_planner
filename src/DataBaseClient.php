<?php

namespace Dirst\OkPoster;

use MeekroDB;
use Psr\Log\AbstractLogger;

/**
 * Database client class.
 *
 * @author Dirst <dirst.guy@gmail.com>
 * @version 1.0
 */
class DataBaseClient
{
    // Tables.
    const TASK_TABLE = "ok_poster_tasks";
    const ACCOUNTS_TABLE = "ok_poster_accounts";
    const POSTS_TABLE = "ok_poster_posts";


    // @var MeekroDb object.
    protected $db;


    /**
     * Create database connection.
     */
    public function __construct()
    {
        $this->dbConnect();
    }

    /**
     * Set up database connection.
     */
    protected function dbConnect()
    {
        $dbSettings = file_get_contents(__DIR__ . "/../db.settings");
        $dbSettings = explode("@", $dbSettings);

        $userPass = explode(":", $dbSettings[0]);
        $hostDb = explode(":", $dbSettings[1]);
        
        // Connect.
        $this->db = new MeekroDB('localhost', $userPass[0], $userPass[1], $hostDb[1], null, "utf8");
        
        // Catch exceptions ability turning on.
        $this->db->success_handler = false;
        $this->db->error_handler = true;
    }

    /**
     * Returns Database connection.
     *
     * @return MeekroDb
     *   Database object.
     */
    public function getDatabaseConnection()
    {
        return $this->db;
    }
}
