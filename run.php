<?php

/** 
 * @file
 * 
 * Code that runs planner routines. 
 */

require __DIR__ . "/vendor/autoload.php";
$poster = new Dirst\OkPoster\OkPosterBase();
$poster->run();